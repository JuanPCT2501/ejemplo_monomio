/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Modelo.Monomio;
import Modelo.Persona;

/**
 * Clase de prueba para Objetos Monomios
 *
 * @author madarme
 */
public class TestMonomio {

    public static void main(String[] args) {

        Monomio m1 = new Monomio(23, 'x', -2);
        Monomio m2 = new Monomio(23, 'x', -2);
        System.out.println(m1.toString());
        System.out.println(m2.toString());

        Persona nadie = new Persona();

        boolean sonIguales = m1.equals(m2);
        if (sonIguales) {
            System.out.println(m1.toString() + " ES IGUAL A :" + m2.toString());
        } else {
            System.out.println(m1.toString() + " NO SON IGUALES:" + m2.toString());
        }

        try {
            //int c = m1.compareTo(nadie);
            int c2 = m1.compareTo(m2);
            if (c2 == 0) {
                System.out.println(m1.toString() + " ES IGUAL A :" + m2.toString());
            } else if (c2 > 0) {
                System.out.println(m1.toString() + " ES MAYOR:" + m2.toString());
            } else {
                System.out.println(m1.toString() + " ES MENOR:" + m2.toString());
            }
            System.out.println("comparador:" + c2);
        } catch (Exception e) {
            System.err.println("Error :( -->" + e.getMessage());
        }

    }

}
