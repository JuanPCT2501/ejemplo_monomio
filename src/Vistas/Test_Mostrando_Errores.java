/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Modelo.Monomio;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author madar
 */
public class Test_Mostrando_Errores {

    public static void main(String[] args) {
        //public Monomio(float coeficiente, char literal, int exponente)
        Monomio x = new Monomio(leerFloat("Digite coeficiente:"), leerChar("Digite literal:"), leerInt("Digite exp:"));
        System.out.println("Su monomio es:" + x.toString());

    }

    private static int leerInt(String msg) {

        //Ejemplifico la captura de manera general:
        try {
            System.out.println(msg);
            Scanner in = new Scanner(System.in);
            return (in.nextInt());
        } catch (Exception e) {
            System.err.println("No es un entero:" + e.getMessage());
            return leerInt(msg);
            //return -1;
        }
    }

    private static float leerFloat(String msg) {
        //opcional
        try {
            System.out.println(msg);
            Scanner in = new Scanner(System.in);
            return (in.nextFloat());
        } catch (java.util.InputMismatchException e) {
            //Esté error es especìfico
            System.out.println("Se ha producido un error , no es un Float:" + e.getMessage());
            return leerFloat(msg);
        }
    }

    private static char leerChar(String msj) {
        System.out.println(msj);
        Scanner in = new Scanner(System.in);
        char letra = in.nextLine().charAt(0);
        try {
            validarChar(letra);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return leerChar(msj);
        }

        return (letra);
    }

    /**
     * Vamos a lanzar una excepción OBLIGATORIA
     *
     * @param caracter un caracter a validar entre a y z or A y Z
     *
     */
    private static void validarChar(char caracter) throws Exception {
        boolean esValido1 = (caracter >= 'a' && caracter <= 'z');
        boolean esValido2 = (caracter >= 'A' && caracter <= 'Z');
        if (!esValido1 || !esValido2) {
            throw new Exception("No es una letra");
        }

    }
}
