
package Vistas;

import Modelo.Persona;

/**
 * Clase de prueba para Objetos Persona
 *
 * @author JuanPCT
 */
public class TestPersona {
    
    public static void main(String[] args) {
        Persona per1 = new Persona(10050,(short)2001,(short)6,(short)19,(short)20,(short)35,(short)53,"Juan");
        Persona per2 = new Persona(10051,(short)2002,(short)3,(short)22,(short)15,(short)24,(short)3,"Pablo");
        
        boolean sonIguales = per1.equals(per2);
        if (sonIguales) {
            System.out.println(per1.toString() + " \nTiene la misma fecha de nacimiento que \n" + per2.toString());
        } else {
            System.out.println(per1.toString() + " \nNo tiene la misma fecha de nacimiento que \n" + per2.toString());
        }
        int c2 = per1.compareTo(per2);
            if (c2 == 0) {
                System.out.println(per1.toString() + "\n ES IGUAL A :\n" + per2.toString());
            } else if (c2 < 0) {
                System.out.println(per1.toString() + "\n ES MAYOR QUE:\n" + per2.toString());
            } else {
                System.out.println(per1.toString() + "\n ES MENOR QUE:\n" + per2.toString());
            }
            System.out.println("comparador:" + c2);
    }
}
