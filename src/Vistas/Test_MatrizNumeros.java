/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vistas;

import Negocio.*;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author madar
 */
public class Test_MatrizNumeros {
    
    public static void main(String[] args) {
        int cantFilas = leerEntero("Cuantas filas desea?:");
        int cantCol = leerEntero("Cuantas columnas máximo por fila desea?:");
        
        Random r = new Random();
        MatrizNumeros myMatriz = new MatrizNumeros(cantFilas);
        for (int i = 0; i < myMatriz.length(); i++) {
            ListaNumeros x = new ListaNumeros(r.nextInt(cantCol) + 1);
            for (int j = 0; j < x.length(); j++) {
                x.adicionar(j, r.nextInt(100));
            }
            //Adicionar la lista de numeros a la matriz
            myMatriz.adicionar(i, x);
        }
        System.out.println("Mi matriz es:\n" + myMatriz.toString());
    }
    
    
    
    
    private static int leerEntero(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextInt();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un Float");
            return leerEntero(msg);
        }

    }
}
