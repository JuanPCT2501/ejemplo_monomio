/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 * Está clase represneta un monomio con un sólo literal
 *
 * @author madarme
 */
public class Monomio implements Comparable {

    private float coeficiente;
    private char literal;
    private int exponente;

    /**
     * Constructor para un monomio vacío
     */
    public Monomio() {
    }

    /**
     * Constructor de un monomio con sus datos básicos
     *
     * @param coeficiente un float con el dato del coeficiente
     * @param literal un char con el dato del literal
     * @param exponente un float con el exponente(con o sin signo)
     */
    public Monomio(float coeficiente, char literal, int exponente) {
        this.coeficiente = coeficiente;
        this.literal = literal;
        this.exponente = exponente;
    }

    /**
     * Obtiene el coeficiente del monomio
     *
     * @return un float con el coeficiente
     */
    public float getCoeficiente() {
        return coeficiente;
    }

    /**
     * Actualiza el valor del coeficiente
     *
     * @param coeficiente un float con el nuevo valor del coeficiente
     */
    public void setCoeficiente(float coeficiente) {
        this.coeficiente = coeficiente;
    }

    public char getLiteral() {
        return literal;
    }

    public void setLiteral(char literal) {
        this.literal = literal;
    }

    public int getExponente() {
        return exponente;
    }

    public void setExponente(int exponente) {
        this.exponente = exponente;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + Float.floatToIntBits(this.coeficiente);
        hash = 89 * hash + this.literal;
        hash = 89 * hash + this.exponente;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Monomio other = (Monomio) obj; //MiddlerCasting-> Object se transforma una clase del mundo

        if (Float.floatToIntBits(this.coeficiente) != Float.floatToIntBits(other.coeficiente)) {
            return false;
        }
        if (this.literal != other.literal) {
            return false;
        }
        if (this.exponente != other.exponente) {
            return false;
        }
        return true;
    }

    /**
     * Obtiene un monomio de la forma coef+literal+exp
     * @return Una cadena con el monomio
     */
    @Override
    public String toString() {
        return coeficiente + " " + literal + "^" + exponente;
    }

    /**
     * SIEMPRE ES UNA RESTA(GENERAL)
     *
     * @param obj el segundo monomio que deseo comparar
     * @return 0 si son iguales; MAYOR 0 si objeto actual es  MAYOR a segundo objeto o menor a 0
     * en caso contrario
     */
    @Override
    public int compareTo(Object obj) {
        //1. Tipo:
        if (this == obj) {
            return 0;
        }
        if (obj == null) {
            // error
            //Adelantandonos 1 semana:
            //Lanzar una Excepción
            throw new RuntimeException("UD paso el segundo objeto nulo");
        }
        if (getClass() != obj.getClass()) {
            throw new RuntimeException("UD está realizando comparaciones con objetos de clases distintas");
        }
        //Lógica de saber cuando un monomio es <,=, >
        //Restas:
        final Monomio other = (Monomio) obj; //MiddlerCasting-> Object se transforma una clase del mundo
        float coef = this.coeficiente - other.coeficiente;
        float exp = this.exponente - other.exponente;
        int literal = this.literal - other.literal;
        /**
         * Casos de comparación del monomio:
         */
        if (coef == 0 && exp == 0 && literal == 0) {
            return 0;
        }

        /**
         * 14x2 4x2
         */
        if (literal == 0) {
            if (exp == 0) {
                if (coef == 0) {
                    return 0;
                } else {
                    return (int) coef;
                }
            } else {
                //12x2    12x3
                return (int) exp;
            }
        } else {
            // 12y2  vs 12x2
            return literal;
        }

    }

}
