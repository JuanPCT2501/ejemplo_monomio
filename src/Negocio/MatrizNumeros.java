 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

/**
 *
 * @author madar
 */
public class MatrizNumeros {

    private ListaNumeros[] filas;

    public MatrizNumeros() {
    }
    
    
    public MatrizNumeros(int cantFilas) {
        
        this.filas=new ListaNumeros[cantFilas];
    }

    public ListaNumeros[] getFilas() {
        return filas;
    }

    public void setFilas(ListaNumeros[] filas) {
        this.filas = filas;
    }

    private void validar(int i) {
        if (i < 0 || i >= this.filas.length) {
            throw new RuntimeException("Índice de la fila fuera de rango:" + i);
        }
    }

    public ListaNumeros getElementoVector(int i) {

        this.validar(i);
        return this.filas[i];
    }

    public void adicionar(int i, ListaNumeros listaNueva) {
        this.validar(i);
        this.filas[i] = listaNueva;
    }

    @Override
    public String toString() {
        String msg = "";
        for (ListaNumeros lista : this.filas) {
            msg += lista.toString() + "\n";

        }
        return msg;
    }

    /**
     * Retorna la cantidad de filas
     * @return un entro con la cantidad de filas
     */
     public int length() {
        return this.filas.length;
    }

     /**
      * Retorna el tipo de matriz:
      *  Cuadrada, rectangular o dispersa
      * @return una cadena con el tipo de matriz
      */
     public String getTipo()
     {
        if (this==null) {
            throw new RuntimeException("La matriz esta vacia");
        }
        int caso=0;
        int columnas=0;
        for (ListaNumeros lista : this.filas) {
            
            if(this.length()==lista.length()){
                if(caso==0||caso==1)
                    caso=1;                
            }
            else{
               if(caso==1)
                    caso=3;
               else{
                    if(caso==0){
                       columnas=lista.length();
                   }
                   else{
                       if(columnas==lista.length()){
                           caso=2;
                           columnas=lista.length();
                        }
                        else
                           caso=3;
                   }
                }
            }            
        }
        switch (caso) {
            case 1:
                return "Cuadrada";
            case 2:
                return "Rectangular";
            case 3:
                return "Dispersa";
        }
        return null;
     }
}
